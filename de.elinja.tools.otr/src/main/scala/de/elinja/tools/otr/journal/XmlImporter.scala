package de.elinja.tools.otr.journal

import java.io.File
import scala.xml.XML
import scala.xml.Elem
import scala.xml.Node
import scala.xml.NodeSeq

object XmlImporter {

  def importFile(filename: String) = {
    if (new File(filename).exists()) {
      createItemsFromXml(XML.loadFile(filename))
    } else {
      Nil
    }
  }

  private def createItemsFromXml(rootNode: Elem): List[JournalItem] = {
    createItemsFromNodeSeq(rootNode \\ "item")
  }

  private def createItemsFromNodeSeq(nodeSeq: NodeSeq): List[JournalItem] = {
    if (nodeSeq.isEmpty) {
      Nil
    } else {
      createItemFromNode(nodeSeq.head) :: createItemsFromNodeSeq(nodeSeq.tail)
    }
  }

  private def createItemFromNode(node: Node): JournalItem = {
    val downloadurl = (node \\ "@downloadurl").text
    val status = (node \\ "@status").text
    new JournalItem(downloadurl, status, false)
  }
}
