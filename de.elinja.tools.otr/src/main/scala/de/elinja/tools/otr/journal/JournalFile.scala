package de.elinja.tools.otr.journal

import java.io.File
import scala.xml.XML
import scala.xml.XML
import scala.xml.Elem
import de.elinja.tools.otr.DownloadStatus._
import scala.xml.Node
import scala.xml.NodeSeq
import scala.xml.Attribute
import scala.xml.Text
import scala.xml.Null
import scala.xml.XML
import de.elinja.tools.otr.DownloadStatus

class JournalFile(filename: String) {

  var items: List[JournalItem] = XmlImporter.importFile(filename)

  def getItemsWithout(downloadUrl: String): List[JournalItem] = {
    items.filter(_.downloadUrl != downloadUrl)
  }

  def getStatusOf(downloadUrl: String): DownloadStatus = {
    val matchingItems = items.filter(item => item.downloadUrl == downloadUrl)

    if (matchingItems.isEmpty) {
      NEW
    } else {
      try {
        DownloadStatus.withName(matchingItems.head.status)
      } catch {
        case e: NoSuchElementException => NEW;
      }
    }
  }

  def updateItem(downloadUrl: String, status: DownloadStatus) = {
    items = new JournalItem(downloadUrl, status.toString(), true) :: getItemsWithout(downloadUrl)
  }

  def getListOfUpdatedItems(): List[JournalItem] = {
    items.filter(_.updated)
  }

  def save() = {
    XmlExporter.saveAsXml(filename, items)
  }
}
