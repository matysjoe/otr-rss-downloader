package de.elinja.tools.otr.journal

import scala.xml.Node
import scala.xml.NodeSeq
import scala.xml.Text
import scala.xml.Attribute
import scala.xml.Null
import java.io.File
import scala.xml.XML

object XmlExporter {

  def saveAsXml(filename: String, items: List[JournalItem]) = {
    // Save in a tmp file first and rename it to the original filename
    // to avoid having a broken file when the app crashes
    val tmpfile = filename + ".tmp"
    XML.save(tmpfile, exportToXml(items))
    val f = new File(tmpfile)
    f.renameTo(new File(filename))
  }

  def exportToXml(items: List[JournalItem]): Node = {
    val itemsAsXml = createXmlForItems(items)
    <journal> { itemsAsXml } </journal>
  }

  private def createXmlForItems(items: List[JournalItem]): NodeSeq = {
    if (items.isEmpty) {
      Nil
    } else {
      createXmlForItem(items.head) ++ createXmlForItems(items.tail)
    }
  }

  private def createXmlForItem(item: JournalItem): Node = {
    <item/> % //
      Attribute(None, "downloadurl", Text(item.downloadUrl), Null) % //
      Attribute(None, "status", Text(item.status), Null)
  }

}
