package de.elinja.tools.otr

import java.io.StringWriter
import com.github.kevinsawicki.http.HttpRequest
import scala.xml.XML
import java.io.IOException
import scala.xml.Elem
import com.typesafe.config.Config
import java.text.MessageFormat

class RssReader(val cfg: Config, val targetDir: String) {

  def getDownloadRunnables(): List[DownloadRunnable] = {
    createDownloadRunnablesForItems(getItemsFromRss())
  }

  private def loadRssXmlFromUrl(url: String) = {
    val request = HttpRequest.get(url);
    if (request.ok()) {
      val sw = new StringWriter();
      request.receive(sw);
      XML.loadString(sw.toString());
    } else {
      throw new IOException("Error downloading RSS feed - returncode " + request.code())
    }
  }

  private def loadRssXml(): Elem = {
    val url = cfg.getString("url")
    println(MessageFormat.format(cfg.getString("messages.loadingrss"), url));

    if (url.startsWith("http://") || url.startsWith("https://")) {
      loadRssXmlFromUrl(url)
    } else {
      XML.loadFile(url);
    }
  }

  private def getItemsFromRss() = {
    (loadRssXml() \ "channel" \ "item").toList
  }

  private def createDownloadRunnablesForItems(items: List[scala.xml.Node]): List[DownloadRunnable] = {
    if (items.isEmpty) {
      Nil
    } else {
      createDownloadRunnableForItem(targetDir, items.head) :: createDownloadRunnablesForItems(items.tail)
    }
  }

  private def createDownloadRunnableForItem(targetDir: String, item: scala.xml.Node): DownloadRunnable = {
    val targetFilename = (item \ "filename").text
    val downloadUrl = (item \ "download_url").text
    return new DownloadRunnable(downloadUrl, targetDir + "/" + targetFilename)
  }
}
