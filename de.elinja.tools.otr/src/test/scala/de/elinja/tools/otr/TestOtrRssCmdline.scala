
package de.elinja.tools.otr
import org.scalatest._
import scala.collection.mutable.Stack
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import java.nio.file.Files
import java.nio.file.Paths

@RunWith(classOf[JUnitRunner])
class TestOtrRssCmdline extends FlatSpec {

  "Calling Cmdline for test1" should "copy file1 and file2 to build/tmp/test" in {

    val destDir = Paths.get("build/tmp/test1")
    val file1 = Paths.get(destDir.toString(), "test1_file1.txt");
    val file2 = Paths.get(destDir.toString(), "test1_file2.txt");
    val journalFile = Paths.get(destDir.toString(), ".test1-journal.xml");
    val lockFile = Paths.get(destDir.toString(), "test1.lck");

    Files.deleteIfExists(file1)
    Files.deleteIfExists(file2)
    Files.deleteIfExists(journalFile)
    Files.deleteIfExists(destDir)

    Files.createDirectories(destDir)

    OtrRssCmdline.main(Array("src/test/resources/test1/test1.conf"))

    assert(file1.toFile().exists())
    assert(file2.toFile().exists());
  }
}
